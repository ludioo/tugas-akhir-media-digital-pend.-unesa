﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BtnHandler : MonoBehaviour
{
    private string name;
    
    void Start()
    {
        name = gameObject.GetComponentInChildren<Text>().text;
    }

    public void onClick() {
        // Hide all 3D Objects
        List<GameObject> objects = GameUtils.findObjectsWithTag("3d");
        
        foreach (var item in objects)
        {
            item.SetActive(false);
        }

        GameObject objectDest = GameUtils.findObject(name);
        objectDest.SetActive(true);
    }
}
