﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object3D {

    public Object3D(string name, string model) {
        this.name = name;
        this.model = model;
    }
    
    public string name { get; set; }
    public string model { get; set; }
}