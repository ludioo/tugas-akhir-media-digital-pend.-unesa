﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropManager : MonoBehaviour
{
    public GameObject drop;
    public GameObject imageObject;
    public GameObject spawnPoint;
    public GameObject prefab;

    private List<DownItem> downItems;

    // Start is called before the first frame update
    void Start()
    {
        downItems = new List<DownItem>();

        initData();

        Dropdown dropdown = drop.GetComponent<Dropdown>();
        dropdown.onValueChanged.AddListener(delegate {
            valueChanged(dropdown);
        });
    }

    void initData() {
        List<Object3D> ccl4 = new List<Object3D>();
        ccl4.Add(new Object3D("CCL4 dalam CCL4", "ccl4_object"));
        downItems.Add(new DownItem("ccl4_dalam_ccl4", ccl4));

        List<Object3D> ccl4cs2 = new List<Object3D>();
        ccl4cs2.Add(new Object3D("CCL4 dalam CS2", "ccl4cs2_object"));
        downItems.Add(new DownItem("ccl4_dalam_cs2", ccl4cs2));

        List<Object3D> cl2 = new List<Object3D>();
        cl2.Add(new Object3D("Cl2 dalam Cl2", "cl2_object"));
        downItems.Add(new DownItem("cl2_dalam_cl2", cl2));

        List<Object3D> ch4 = new List<Object3D>();
        ch4.Add(new Object3D("Ch4 dalam Ch4", "ch4_object"));
        downItems.Add(new DownItem("ch4_dalam_ch4", ch4));

        List<Object3D> i2 = new List<Object3D>();
        i2.Add(new Object3D("I2 dalam CH2OH", "i2+ch2oh_object"));
        downItems.Add(new DownItem("i2_dalam_ch2oh", i2));

        List<Object3D> o2 = new List<Object3D>();
        o2.Add(new Object3D("O2 dalam H2O", "o2+h2o_object"));
        downItems.Add(new DownItem("o2_dalam_h2o", o2));

        List<Object3D> h2co = new List<Object3D>();
        h2co.Add(new Object3D("H2CO dalam H2O", "h2co+h2o_object"));
        downItems.Add(new DownItem("h2co_dalam_h2o", h2co));

        List<Object3D> hcl = new List<Object3D>();
        hcl.Add(new Object3D("HCl dalam HCl", "hcl_object"));
        downItems.Add(new DownItem("hcl_dalam_hcl", hcl));

        List<Object3D> pcl3 = new List<Object3D>();
        pcl3.Add(new Object3D("PCl3 dalam PCl3", "pcl3_object"));
        downItems.Add(new DownItem("pcl3_dalam_pcl3", pcl3));

        List<Object3D> hbr = new List<Object3D>();
        hbr.Add(new Object3D("HBr dalam HBr", "hbr_object"));
        downItems.Add(new DownItem("hbr_dalam_hbr", hbr));

        List<Object3D> hf = new List<Object3D>();
        hf.Add(new Object3D("Hf dalam Hf", "hf_object"));
        downItems.Add(new DownItem("hf_dalam_hf", hf));

        List<Object3D> h2o = new List<Object3D>();
        h2o.Add(new Object3D("Cl2 dalam Cl2", "h2o_object"));
        downItems.Add(new DownItem("h2o", h2o));
    }

    
    void valueChanged(Dropdown down) {
        DownItem item = downItems[down.value];
        Image image = imageObject.GetComponent<Image>();
        image.sprite = Resources.Load<Sprite>(item.name);

        List<Object3D> childs = item.object3d;

        float x = spawnPoint.transform.localPosition.x;
        float y = spawnPoint.transform.localPosition.y;

        clearTexts();
        hide3ds();

        for (int i = 0; i < childs.Count; i++) {
            GameObject txtObject = Instantiate(prefab, new Vector2(x, y), Quaternion.identity) as GameObject;
            txtObject.transform.SetParent(spawnPoint.transform.parent, false);
            txtObject.GetComponent<Text>().text = "Senyawa " + childs[i].name;
            y += 30;
            
            GameUtils.findObject(childs[i].model).SetActive(true);
        }
    }

    void clearTexts()
    {
        GameObject[] texts = GameObject.FindGameObjectsWithTag("Text");
        foreach (var item in texts)
        {
            Destroy(item);
        }
    }

    void hide3ds()
    {
        List<GameObject> objects = GameUtils.findObjectsWithTag("3d");
        foreach (var item in objects)
        {
            item.SetActive(false);
        }
    }
}
