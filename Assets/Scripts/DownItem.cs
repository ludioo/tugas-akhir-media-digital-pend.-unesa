using System.Collections.Generic;

public class DownItem {

    public DownItem(string name, List<Object3D> object3d) {
        this.name = name;
        this.object3d = object3d;
    }
    
    public string name { get; set; }
    public List<Object3D> object3d { get; set; }
}