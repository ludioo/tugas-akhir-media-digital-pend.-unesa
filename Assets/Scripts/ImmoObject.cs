﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class ImmoObject : MonoBehaviour
{
    public Button btnNext;
    SceneScript sceneScript;

    void Start()
    {
        btnNext.onClick.AddListener(nextScene);
    }

    void nextScene() {
        GameObject[] games = GameObject.FindGameObjectsWithTag("Toggle");

        foreach (var item in games)
        {
            if (item.GetComponent<Toggle>().isOn) {
                Text text = item.GetComponent<Toggle>().GetComponentInChildren<Text>();
                
                Ehe.answers.Add(text.text);
            }
        }

        sceneScript.SceneForward();
    }
}
