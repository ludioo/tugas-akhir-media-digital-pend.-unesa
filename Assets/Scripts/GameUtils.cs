﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUtils
{
    public static GameObject findObjectWithTag(string tagName)
    {
        GameObject[] objects = Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[];
        foreach (var item in objects)
        {
            if (item.tag.Equals(tagName)) {
                return item.gameObject;
            }
        }

        return null;
    }

    public static List<GameObject> findObjectsWithTag(string tagName)
    {
        GameObject[] objects = Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[];
        List<GameObject> results = new List<GameObject>();

        foreach (var item in objects)
        {
            if (item.tag.Equals(tagName)) {
                results.Add(item.gameObject);
            }
        }

        return results;
    }

    public static GameObject findObject(string name)
    {
        GameObject[] objects = Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[];
        foreach (var item in objects)
        {
            if (item.name.Equals(name)) {
                return item.gameObject;
            }
        }

        return null;
    }
}