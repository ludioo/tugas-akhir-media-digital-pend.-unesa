﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneScript : MonoBehaviour
{
    int sceneNumber;
    Scene scene;

    public void Start()
    {
        scene = SceneManager.GetActiveScene();
        sceneNumber = scene.buildIndex;
        
    }
    public void SceneForward()
    {
        sceneNumber = sceneNumber + 1;
        SceneManager.LoadScene(sceneNumber, LoadSceneMode.Single);
    }

    public void SceneBackward()
    {
        sceneNumber = sceneNumber - 1;
        SceneManager.LoadScene(sceneNumber, LoadSceneMode.Single);
    }
}
